@extends('admin.layouts.master')
@section('content')

<div class="col-12 col-md-12 col-lg-12">
  <div class="card">
    <div class="card-header">
      <h4>Tambah Data Barang</h4>
    </div>
    <form action="{{ route('post_barang') }}" method="post">
      @csrf
      <div class="card-body">
        <div class="form-group">
          <label>Nama Barang</label>
          <input type="text" class="form-control" name="nama_barang" id="nama_barang">
        </div>
        <div class="form-group">
          <label>Harga Satuan</label>
          <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text">
                Rp.
              </div>
            </div>
            <input type="text" class="form-control currency" name="harga_barang" id="harga_barang">
          </div>
        </div>
        <div class="form-group">
          <label>Stok</label>
          <input type="number" class="form-control" name="stok_barang" id="stok_barang">
        </div>
        <div class="form-group">
          <label>ID Suplier</label>
          <select class="form-control" type="text" name="id_suplier" id="id_suplier">
            <option set>Pilih ID Suplier</option>
            @foreach($data as $row)
            <option value="{{ $row-> id_suplier }}">{{ $row-> id_suplier }}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label>Show or Hide</label>
          <select class="form-control" type="number" name="is_active" id="is_active">
            <option set>Pilih ... </option>
            <option value="1">1</option>
            <option value="0">0</option>
          </select>
        </div>
        <div class="form-group">
          <label>Keterangan</label>
          <input type="teks" class="form-control" name="keterangan" id="keterangan">
        </div>
        <input class="btn btn-info btn-sm" type="button" value="Kembali">
        <input class="btn btn-success btn-sm" type="submit" value="Simpan">
      </div>
    </form>
  </div>
</div>
@endsection