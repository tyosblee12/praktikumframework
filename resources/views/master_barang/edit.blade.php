<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tambah Data Barang</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <div class="card mt-5 align-items-center">
            <div class="card-body col-5">
                <h2 class="text-center text-info p-2"><u>Edit Data Barang</u></h2><br>
                <form action="{{ route('update_data', $data->kode_barang) }}" method="post">
                    @csrf
                    <div class="form-group col-12">
                        Nama Barang<br>
                        <input class="form-control bg-gray-100 mt-2" type="text" name="nama_barang" id="nama_barang"
                            value="{{ $data->nama_barang }}"><br>
                        Harga Barang<br>
                        <input class="form-control bg-gray-100 mt-2" type="number" name="harga_barang" id="harga_barang"
                            value="{{ $data->harga_satuan }}"><br>
                        Stok<br>
                        <input class="form-control bg-gray-100 mt-2" type="number" name="stok_barang" id="stok_barang"
                            value="{{ $data->stok }}"><br>
                        Keterangan<br>
                        <input class="form-control bg-gray-100 mt-2" type="text" name="keterangan" id="keterangan"
                            value="{{ $data->keterangan }}"><br>

                        <input type="button" class="btn btn-info btn-sm" value="Kembali">

                        <input type="submit" class="btn btn-success btn-sm" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

</html>