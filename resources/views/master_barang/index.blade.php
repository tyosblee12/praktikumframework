<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Barang</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
</head>

<body>
    <div class="container">
        <div class="card mt-5">
            <div class="card-body">
                <a href="{{ route('create_barang') }}">Tambah Data</a> <a href="{{ route('trash') }}"
                    class="float-right text-danger"><i class="fa fa-trash" aria-hidden="true"></i> Tong
                    Sampah</a><br><br>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>
                            <th>Harga Satuan</th>
                            <th>Stok</th>
                            <th>Keterangan</th>
                            <th width="30%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $i=1;
                        @endphp
                        @foreach($data as $row)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{ $row->nama_barang}}</td>
                            <td>{{ $row->harga_satuan}}</td>
                            <td>{{ $row->stok}}</td>
                            <td>{{ $row->keterangan}}</td>
                            <td>
                                <a href="{{ route('edit_data',$row->kode_barang) }}"
                                    class="btn btn-success btn-sm">Edit</a>&nbsp
                                <a href="{{ route('delete_data',$row->kode_barang) }}"
                                    class="btn btn-danger btn-sm">Delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @if(session()->has('error'))
                {{session()->get('error')}}
                @endif
                @if(session()->has('success'))
                {{session()->get('success')}}
                @endif
            </div>
        </div>
    </div>
</body>

</html>