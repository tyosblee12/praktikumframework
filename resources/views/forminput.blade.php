<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h2>Penambahan</h2>
    <form action="{{ route('tambah') }}" method="post">
        @csrf
        <input type="teks" name="variablesatu" id="variablesatu"></input>
        <input type="teks" name="variabledua" id="variabledua"></input>
        <input type="submit" name="tambah" id="tambah" value="tambah"></input>
    </form>
    <h2>Perkalian</h2>
    <form action="{{ route('kali') }}" method="post">
        @csrf
        <input type="teks" name="variablekali1" id="variablekali1"></input>
        <input type="teks" name="variablekali2" id="variablekali2"></input>
        <input type="submit" name="kali" id="kali" value="kali"></input>
    </form>
    <h2>Pembagian</h2>
    <form action="{{ route('bagi') }}" method="post">
        @csrf
        <input type="teks" name="variablebagi1" id="variablebagi1"></input>
        <input type="teks" name="variablebagi2" id="variablebagi2"></input>
        <input type="submit" name="bagi" id="bagi" value="bagi"></input>
    </form>
    <h2>Pengurangan</h2>
    <form action="{{ route('kurang') }}" method="post">
        @csrf
        <input type="teks" name="variablekurang1" id="variablekurang1"></input>
        <input type="teks" name="variablekurang2" id="variablekurang2"></input>
        <input type="submit" name="kurang" id="kurang" value="kurang"></input>
    </form>
</body>

</html>