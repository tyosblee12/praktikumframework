<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <link href="{!! asset('assets/css/styleaja.css') !!}" rel="stylesheet">

</head>

<body>
    <div class="bungkus">
        <div class="row">
            <div class="col-7 animated--grow-in">
                <div class="card shadow">
                    <div class="card-header mb-4">
                        <h2 class="m-0 teksoy font-weight-bold text-dark">Pemrograman Framework</h2>
                    </div>

                    <div class="col-10 ml-5">
                        <form class="card shadow mt-2 mb-2" method="post" action="{{ route('hitungbb') }}">
                            {{csrf_field()}}
                            <div class="row p-3">
                                <label class="font-weight-bold form-control-lg text-warning ml-5 mr-5">Menghitung Berat
                                    Badan <div class="dropdown-divider"></div> </label>

                                <div class="col-md-12 mt-3 ml-5 mr-5">
                                    <label class="font-weight-bold">Jenis Kelamin</label><br>

                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="jk" id="wanita"
                                            value="wanita">
                                        <label class="form-check-label" for="wanita">Wanita</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="jk" id="pria" value="pria">
                                        <label class="form-check-label" for="wanita">pria</label>
                                    </div>
                                    <br><br>
                                    <label for="name" class="font-weight-bold">Tinggi Badan (CM) </label>
                                    <input class="form-control col-10" type="text" name="tinggi" id="tinggi"
                                        require="tinggi" value="" placeholder="Masukkan Tinggi Badan">
                                    <br>
                                    <label for="name" class="font-weight-bold">Berat Badan (KG) </label>
                                    <input class="form-control col-10" type="text" name="berat" id="berat"
                                        require="berat" value="" placeholder="Masukkan Berat Badan">

                                    <div class="form-group mt-4">
                                        <button class="btn btn-sm btn-primary col-10 font-weight-bold"
                                            type="submit">Check Berat Badan</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                @if (session('status'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('status') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                            </div>
                        </form>
                    </div>

                    <div class="col-10 ml-5">
                        <form class="card shadow align-items-center justify-content-between mb-5" method="post"
                            action="{{ route('age') }}">
                            @csrf
                            <div class="row p-3">
                                <label class="font-weight-bold form-control-lg text-warning">Menghitung Usia<div
                                        class="dropdown-divider"></div></label>
                                <div class="col-md-12 mt-3">
                                    <label class="font-weight-bold">Tanggal Lahir</label><br>

                                    <input class="form-control col-12" type="date" name="tgl_lahir" id="tgl_lahir"
                                        require="tgl_lahir" value="" placeholder="Masukkan Tanggal Lahir Anda">

                                    <div class="form-group mt-4">
                                        <button class="btn btn-sm btn-primary col-12 font-weight-bold"
                                            type="submit">Check Usia Anda</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                @if (session('statusdua'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('statusdua') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>