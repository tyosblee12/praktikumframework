<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BarangModel extends Model
{
    //

    protected $table = 'barang';

    protected $fillable = ['nama_barang', 'harga_satuan', 'stok', 'keterangan', 'id_suplier', 'is_active'];

    // protected $dates = ['deleted_at'];

    public function haveSuplier()
    {
        return $this->belongsTo(SuplierModel::class, 'id_suplier', 'id_suplier');
    }
}