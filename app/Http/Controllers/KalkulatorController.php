<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KalkulatorController extends Controller
{
    //
    public function index()
    {
        return view('forminput');
    }
    public function tambah(Request $request)
    {
        $a = $request->variablesatu;
        $b = $request->variabledua;

        $c = $a + $b;

        return $c;
    }
    public function kali(Request $request)
    {
        $a = $request->variablekali1;
        $b = $request->variablekali2;

        $c = $a * $b;

        return $c;
    }
    public function bagi(Request $request)
    {
        $a = $request->variablebagi1;
        $b = $request->variablebagi2;

        $c = $a / $b;

        return $c;
    }
    public function kurang(Request $request)
    {
        $a = $request->variablekurang1;
        $b = $request->variablekurang2;

        $c = $a - $b;

        return $c;
    }
}